import React, { useState } from 'react';
import styles from './Submit.module.css';

function SubmitForm() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [emailError, setEmailError] = useState(false);
  const [passwordError, setPasswordError] = useState(false);

  const handleSubmit = (event) => {
    event.preventDefault();
    if (email === '') {
      setEmailError(true);
    }
    if (password === '') {
      setPasswordError(true);
    }
  };

  return (
    <div className={styles.sign}>
      <form className={styles.form} onSubmit={handleSubmit}>
        <p> Your email </p>
        {emailError && <p style={{ color: 'red' }}></p>}
        <input
          className={styles.input}
          type='email'
          style={{
          
            backgroundColor: emailError ? 'white' : 'white',
            borderColor: emailError ? 'red' : '1px solid #cccccc;',
          }}
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <p> Your password</p>
        {passwordError && (
          <p style={{ color: 'red' }}></p>
        )}
        <input
          className={styles.input}
          type='password'
          style={{
            backgroundColor: passwordError ? 'white' : 'white',
            borderColor: passwordError ? 'red' : ' 1px solid #cccccc;',
          }}
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <button className={styles.login}>
          Submit
        </button>
      </form>
    </div>
  );
}
export default SubmitForm;