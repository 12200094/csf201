import React, {useState} from "react";

export default function LogicalError(){
    const [count, setCount] = useState(1);

    function handleClick(){
        setCount(count + 1);
        console.log(count)
    }
    return(
        <div>
            <h1> Count: {count} </h1>
            <button onClick={handleClick}>
                Click Me to Increment
            </button>
        </div>
    );
}

