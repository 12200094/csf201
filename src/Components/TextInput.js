import React, {useState, Button} from 'react';
import LoginForm from './LoginForm';

function TextInput(){
    const[inputEmail, setEmail] = useState('');
    const[inputPassword, setPassword] = useState('');
    const[isValid, setIsValid] = useState(true);

    const handleInputEmailChange = (event) => {
        setEmail(event.target.value);

        //perform validation on the input value
        setIsValid(event.target.value.trim().length > 0);
    }

    const handleInputPasswordChange = (event) => {
        setPassword(event.target.value);

        setIsValid(event.target.value.trim().length > 0);
    }
    return (
        <div>
            <LoginForm>
                <p>Your Email</p>
                <TextInput
                    isValid={isValid}
                    inputConfig={{
                        type: 'email',
                        value: inputEmail,
                        onChange: handleInputEmailChange,
                        required: true
                    }}
                /> <br></br>
                <p>Your Password</p>
                <TextInput
                    isValid={isValid}
                    inputConfig={{
                        type: 'password',
                        value: inputPassword,
                        onCange: handleInputPasswordChange,
                        required: true
                    }}
                />
                <Button> Submit </Button>
            </LoginForm>
        </div>   
    );
}
export default TextInput;