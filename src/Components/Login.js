import React, {useState} from 'react';


export default function Login(){
    const[inputEmail, setInputEmail] = useState('');
    const[inputPassword, setInputPassword] = useState('');
    const[emailIsValid, setEmailIsValid] = useState(true);
    const[passwordIsValid, setPasswordIsValid] = useState(true);

    const handleSubmit = (event) => {
        event.preventDefault();
        validateInputEmail();
        validateInputPassword();
    };

    const validateInputEmail = () => {
        if(inputEmail.trim() == ""){
            setEmailIsValid(false);
        }else{
            setEmailIsValid(true);
        }
    };

    const validateInputPassword = () => {
        if(inputPassword.trim() == ""){
            setPasswordIsValid(false);
        }else{
            setPasswordIsValid(true);
        }
    };

    return(
        <form onSubmit={handleSubmit}>
            <label>
                Email: 
                <input
                    type='email'
                    value={inputEmail}
                    onChange={(event) => setInputEmail(event.target.value)}
                    className={emailIsValid ? "valid" : "invalid"}
                />
                {!emailIsValid && (
                    <span className='error'>
                        Please enter your email address
                    </span>
                )}
            </label>
            <br></br>
            <label>
                Password: 
                <input
                    type='password'
                    value = {inputPassword}
                    onChange = {(event) => setInputPassword(event.target.value)}
                    className={passwordIsValid ? "valid" : "invalid"}
                />
                {!passwordIsValid &&(
                    <span className='error'>
                        Please Enter your Password
                    </span>
                )}
            </label>
            <button type='submit'>
                Submit
            </button>
        </form>
    );
}
