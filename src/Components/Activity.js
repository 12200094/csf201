import React, { useState } from 'react';

function Acitivity() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const handleSubmit = (event) => {
    event.preventDefault();

    const emailRegex = /^\S+@\S+\.\S+$/;
    const isValidEmail = emailRegex.test(email);
    const isValidPassword = password.length >= 8;
  
    if (!isValidEmail) {
      setEmail('');
    }
  
    if (!isValidPassword) {
      setPassword('');
    }
  
    // if (isValidEmail && isValidPassword) {
      
    // }
  };
  return (
    <form onSubmit={handleSubmit}>
      <label> Your email </label>
      <input
        type="email"
        id="email"
        name="email"
        value={email}
        onChange={(event) => setEmail(event.target.value)}
      />

      <label>Your password:</label>
      <input
        type="password"
        id="password"
        name="password"
        value={password}
        onChange={(event) => setPassword(event.target.value)}
      />

      <button type="submit">Submit</button>
    </form>
  );
}

export default Acitivity;