import React from "react";

function LoginForm({isValid, inputConfig}){
    let cssClass = 'input-default'
    
    if (!isValid){
        cssClass = 'input-invalid';
    }

    return(
        <input className={cssClass}
                type={inputConfig.type}
                placeholder={inputConfig.placeholder}
                value={inputConfig.value}
                onChange={inputConfig.onChange}
        />
    );
}
export default LoginForm;